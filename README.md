## Name
Sector Command: Common Library

## Description

Common library for Sector Command. Contains general components and functions.

## Usage

    from SC_Commmon import LocationComponent
    from SC_Common import SquareBoundedComponent

    ship = world.create_entity()
    world.add_component(ship, LocationComponent(x=10, y=10))
    world.add_component(ship, SquareBoundedComponent(height=24, width=24))

## License

GPL3+