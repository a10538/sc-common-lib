import unittest
from collections import namedtuple

import assertpy

from SC_Common import get_bounding_box

SquareBound = namedtuple("SquareBound", 'x y h w')
CircBound = namedtuple('CircBound', 'x y r')


class TestBoundingBox(unittest.TestCase):
    def setUp(self) -> None:
        self.s_bounds = SquareBound(4, 4, 4, 4)
        self.s_expected = (2, 2, 6, 6)

        self.c_bounds = CircBound(4, 4, 2)
        self.c_expected = (2, 2, 6, 6)

    def test_square(self):
        actual = get_bounding_box(self.s_bounds.x, self.s_bounds.y, self.s_bounds.h, self.s_bounds.w)

        assertpy.assert_that(actual).is_equal_to(self.s_expected)

    def test_circ(self):
        actual = get_bounding_box(self.c_bounds.x, self.c_bounds.y, self.c_bounds.r)

        assertpy.assert_that(actual).is_equal_to(self.c_expected)


class TestOddSizeBoundingBox(unittest.TestCase):
    def setUp(self) -> None:
        self.s_bounds = SquareBound(4, 4, 5, 5)
        self.s_expected = (2, 2, 7, 7)

        self.c_bounds = CircBound(4, 4, 3)
        self.c_expected = (1, 1, 7, 7)

    def test_square(self):
        actual = get_bounding_box(self.s_bounds.x, self.s_bounds.y, self.s_bounds.h, self.s_bounds.w)

        assertpy.assert_that(actual).is_equal_to(self.s_expected)

    def test_circ(self):
        actual = get_bounding_box(self.c_bounds.x, self.c_bounds.y, self.c_bounds.r)

        assertpy.assert_that(actual).is_equal_to(self.c_expected)


if __name__ == '__main__':
    unittest.main()
