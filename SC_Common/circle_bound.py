from dataclasses import dataclass as component


@component
class CircleBoundComponent(object):
    """A Radius"""
    radius = 0
