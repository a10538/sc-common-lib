import math


def get_bounding_box(x, y, h_r, w=None):
    """
    Get a bounding box tuple. For use with pyqtree.
    :param x: The x position (center of the object)
    :param y: The y position (center of the object)
    :param h_r: The height or radius of the sprite (total height)
    :param w: The width of the sprite (total width)
    :return: (min_x (width/2 rounded down),
        min_y (height/2 rounded down),
        max_x (width/2 rounded up),
        max_y (width/2 rounded up))
    """
    if w is None:
        r = h_r
        return x - r, y - r, x + r, y + r
    else:
        h = h_r
        return x - math.floor(w / 2), y - math.floor(h / 2), x + math.ceil(w / 2), y + math.ceil(h / 2)
