from dataclasses import dataclass as component


@component
class LocationComponent(object):
    """x, y coordinates"""
    x = 0
    y = 0
