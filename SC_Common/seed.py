from dataclasses import dataclass as component


@component
class SeedComponent(object):
    """An RNG seed"""
    seed = 0
