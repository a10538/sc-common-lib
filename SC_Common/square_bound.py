from dataclasses import dataclass as component


@component
class SquareBoundComponent(object):
    """Height and Width"""
    height = 0
    width = 0
