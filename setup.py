import os

from setuptools import setup


def read(file_name):
    with open(os.path.join(os.path.dirname(__file__), file_name), "r") as f:
        return f.read()


setup(
    name="SC_Common",
    version="0.0.1",
    author="Jeffrey Yurkiw",
    author_email="jyurkiw@yahoo.com",
    description="Common components and library functions for Sector Command.",
    license="GPL version 3 or any later version.",
    keywords="",
    url="https://gitlab.com/a10538/sc-common-lib",
    packages=["SC_Common"],
    package_dir={'SC_Common': 'SC_Common'},
    long_description_content_type='text/markdown',
    long_description=read("README.md"),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Games/Entertainment",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
    ],
)
